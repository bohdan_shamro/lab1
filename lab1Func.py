import matplotlib.pyplot as plt
import scipy.stats as stats
import statistics
import numpy as np
import time

def getVectorGenerator(a ,b,n):
	"""
	return vector of n random numbers in range form a to b 
	"""
	return(np.random.randint(a,b,n),a,b)
	
def getMean(vec):
	'''
		return Arithmetic mean (AM) of vector vec
	'''
	suma=0
	for i in range(len(vec)):
		suma+=vec[i]
	return(suma/len(vec))
	
def getMedian(vec):
	'''
	return Median from vec
	'''
#	vec.sort()
	tempVec = vec.copy()
	tempVec.sort()
	print('sorted ',tempVec)
	if len(tempVec)%2 == 0:
		return ( float((tempVec[int(len(tempVec)/2)] + tempVec[int(len(tempVec)/2 - 1)]) / 2 ) )
	return (float(tempVec[round(len(tempVec)/2) ]))


def getMode(vec):
	'''
	return Mode form vec work only with positive numbers
	can be improved to assept negative numbers
	'''
	ans = [] # array where index its a number
	length = len(vec) #the length of vec
	
	for a in range(vec.max()+1): #write 0
		ans.append(0)
		
	for a in range(length): #calculate how much of each number we have
		ans[vec[a]] += 1
	
	mode = []
	number = 0
	 
	for a in range(len(ans)): # find the beggiest
		if ans[a] == number:
			mode.append(a)
		if ans[a] > number:
			number = ans[a]
			mode = []
			mode.append(a)
	return mode
	
def getDispersion(vec):
	''' 
	return Dispersion value
	'''
	mean = getMean(vec)
	d = 0
	for i in range(len(vec)):
		d+=(vec[i]-mean)**2

	return(d/len(vec))
	
	
def plot(xvec, yvec, labelX = "x", labelY = "y"):
	meanX = getMean(xvec)
	meanY = getMean(yvec)
	dispersionX = getDispersion(xvec)
	dispersionY = getDispersion(yvec)
	
	plt.scatter(xvec, yvec, c='b')
	plt.scatter(meanX, meanY, c='r',label='Expected value')
	plt.scatter(dispersionX	, dispersionY, c='g',label='Dispersion')
	plt.xlabel(labelX)
	plt.ylabel(labelY)
	plt.legend()
	plt.show()